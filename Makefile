CFLAGS = -O2 -W -Wall
LDFLAGS = -lncurses

all: matroxset

matrox:	matroxset.o

matroxset.o:	matroxset.c

clean:
	-rm *.o matroxset
